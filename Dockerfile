FROM node:12-alpine

RUN mkdir -p /home/node/app/node_modules && \
mkdir -p /home/node/.aws && \
chown -R node:node /home/node/app && \
chown -R node:node /home/node/.aws

WORKDIR /home/node

COPY aws-key ./.aws/credentials

WORKDIR /home/node/app

COPY package.json ./
COPY yarn.lock ./

USER node

RUN yarn

COPY --chown=node:node . .

EXPOSE 8000

CMD [ "yarn", "start" ]
