import 'reflect-metadata';
import {RedisClient} from 'redis';
import {classToPlain, plainToClass} from 'class-transformer';
import {promisify} from 'util';

const KEY_PREFIX = 'requests'

export enum RequestStatus {
    WAITING = 0,
    PROCESSING = 1,
    FAILED = 2,
    SUCCEED = 3
}

export class NormalizeRequest {
    constructor(
        public readonly requestId: string,
        public readonly inputFileUrl: string,
        public readonly addressColumnIndex: number,
        public readonly status: RequestStatus = RequestStatus.WAITING,
        public readonly outputFileUrl: string = '',
        public readonly createdAt: Date = new Date(),
        public readonly updatedAt: Date = new Date()) {
    }
}

export class RequestRepository {
    constructor(private readonly client: RedisClient) {

    }

    public async createOrUpdate(req: NormalizeRequest): Promise<void> {
        const plainRequest = classToPlain(req);
        await promisify(this.client.set).bind(this.client)(KEY_PREFIX + ':' + req.requestId, JSON.stringify(plainRequest))
    }

    public async get(requestId: string): Promise<NormalizeRequest |  null> {
        try {
            const result = await promisify(this.client.get).bind(this.client)(KEY_PREFIX + ':' + requestId)
            if (!result) {
                return null
            }
            return plainToClass(NormalizeRequest, JSON.parse(result))
        } catch (err) {
            return null
        }
    }

    public async getAll(): Promise<NormalizeRequest[]> {
        try {
            return await new Promise((resolve, reject) => {
                this.client.keys('*', async (err, keys) => {
                    if (err)  {
                        return reject(err)
                    }
                    if(keys){
                        const results = []
                        for(let i=0;i<keys.length;i++) {
                            const key = keys[i]
                            const result = await promisify(this.client.get).bind(this.client)(key)
                            results.push(plainToClass(NormalizeRequest, JSON.parse(result)))
                        }
                        resolve(results.sort((a: NormalizeRequest, b: NormalizeRequest) => {
                            return +b.createdAt - +a.createdAt
                        }))
                    }
                });
            })
        } catch (err) {
            return []
        }
    }
}
