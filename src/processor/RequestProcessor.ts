import {RedisClient} from 'redis';
import {NormalizeRequest, RequestRepository, RequestStatus} from './RequestRepository';
import {CsvParser} from '../parser/CsvParser';
import {v4 as uuidv4} from 'uuid';
import * as stringify from 'csv-stringify';
import {Uploader} from '../uploader/Uploader';
import {TransformCallback} from "stream";
import {PochtaSdk} from './PochtaSdk';
import * as through2Concurrent from 'through2-concurrent';

const REQUESTS_CHANNEL = 'requests'

const INPUT_COLUMN_NAME = 'Исходный адрес'
const ZIP_COLUMN_NAME = 'Почтовый индекс'
const RESULT_COLUMN_NAME = 'Нормализованный адрес'

const CONCURRENT_PROCESSING = 100

export class RequestProcessor {
    constructor(
        private readonly publisher: RedisClient,
        private readonly subscriber: RedisClient,
        private readonly repo: RequestRepository,
        private readonly csvParser: CsvParser,
        private readonly uploader: Uploader,
        private readonly sdk: PochtaSdk
    ) {
        this.registerSubscribers()
    }

    public async uploadFile(filename: string, fileStream: NodeJS.ReadableStream): Promise<{ requestId: string, url: string }> {
        const requestId = this.getRequestId()
        const uploadResult = await this.uploader.uploadInputFile(filename, requestId, fileStream)
        return { requestId, url: uploadResult.Location }
    }

    public async createAndPostRequest(requestId: string, url: string, addressColumnIndex: number): Promise<{ requestId: string, url: string }> {
        await this.repo.createOrUpdate(new NormalizeRequest(requestId, url, addressColumnIndex))
        await this.publisher.publish(REQUESTS_CHANNEL, requestId)
        return { requestId, url }
    }

    private getRequestId(): string {
        return uuidv4()
    }

    private registerSubscribers() {
        this.subscriber.on("message", async (channel:string, message: string) => {
            console.log('redis on message', channel, message)
            // TODO: consider channel name
            try {
                const requestId = message
                const request: NormalizeRequest = await this.repo.get(requestId)
                await this.process(request)
            } catch (err) {
                console.error('Request processing error: ' + err.message)
            }
        })
        this.subscriber.subscribe(REQUESTS_CHANNEL);
    }

    private async process(req: NormalizeRequest): Promise<void> {
        console.log('request process', req)
        try {
            console.time('processing')
            await this.updateRequestStatus(req, RequestStatus.PROCESSING)

            const resultStream = (await this.csvParser.parseRemoteFile(req.inputFileUrl, req.addressColumnIndex))
                .pipe(this.getProcessingStream())
                .pipe(this.getStringifyStream())

            const uploadResult = await this.uploader.uploadResultFile(req, resultStream)
            console.log('finish processing', uploadResult)
            console.timeEnd('processing')

            await this.repo.createOrUpdate(
                new NormalizeRequest(req.requestId, req.inputFileUrl,
                                     req.addressColumnIndex, RequestStatus.SUCCEED, uploadResult.Location,
                                     req.createdAt)
            )
        } catch (err) {
            console.log('error processing', err.message)
            await this.updateRequestStatus(req, RequestStatus.FAILED)
        }
    }

    private async updateRequestStatus({requestId, inputFileUrl, addressColumnIndex, outputFileUrl, createdAt}: NormalizeRequest, status: RequestStatus): Promise<void> {
        await this.repo.createOrUpdate(
            new NormalizeRequest(requestId, inputFileUrl, addressColumnIndex, status, outputFileUrl, createdAt)
        )
    }

    private getStringifyStream() {
        return stringify({ quote: '"', quoted: true, delimiter: ',' })
    }

    private getProcessingStream() {
        const self = this
        let rowIndex = 0
        let inProgressCount = 0

        const makeApiRequest = async (address: string, callback: TransformCallback) => {
            const reqIndex = rowIndex
            console.log('makeApiRequest start ' + reqIndex)
            const normalizeResponse = await self.sdk.normalizeAddress(address)
            console.log('makeApiRequest finish ' + reqIndex)
            if (!normalizeResponse) {
                return callback(null, [address, '-', '-'])
                // return callback(new Error('Missing or invalid address field'))
            }
            return callback(null, [address, normalizeResponse.zip || '-', normalizeResponse.address])
        }

        return through2Concurrent.obj(
            { maxConcurrency: CONCURRENT_PROCESSING },
            async (row: string[], encoding: BufferEncoding, callback: TransformCallback) => {
                rowIndex++
                if (rowIndex === 1) {
                    return callback(null, [INPUT_COLUMN_NAME, ZIP_COLUMN_NAME, RESULT_COLUMN_NAME]);
                }
                await makeApiRequest(row[0], callback)
            })

        // return new Transform({
        //     highWaterMark: 50,
        //     objectMode: true,
        //     async transform(row: string[], encoding: BufferEncoding, callback: TransformCallback) {
        //         rowIndex++
        //         if (rowIndex === 1) {
        //             return callback(null, [INPUT_COLUMN_NAME, ZIP_COLUMN_NAME, RESULT_COLUMN_NAME]);
        //         }
        //         makeApiRequest(row[0], callback)
        //     },
        //     flush(callback: TransformCallback) {
        //         callback();
        //     }
        // });
    }
}
