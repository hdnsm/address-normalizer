import Axios from 'axios'

type PochtaApiResponse = {
    addr: {
        accuracy: string,
        delivery: number,
        index?: number,
        outaddr?: string,
        element?: ({
            val: string,
            tname?: string
        })[]
    }
}

export type NormalizeResponse = {
    // accuracy: string, // TODO change to enum
    address: string
    zip?: string,
}

export class PochtaSdk {
    private baseUrl = 'https://address.pochta.ru'
    private apiKey = '53fb9daa-7f06-481f-aad6-c6a7a58ec0bb'

    public async normalizeAddress(address: string): Promise<NormalizeResponse | null> {
        if (!address) {
            return null
        }
        try {
            const result = await Axios({
                method: 'post',
                url: `${this.baseUrl}/validate/api/v7_1`,
                headers: {
                   'AuthCode': this.apiKey
                },
                data: {
                   version: "6.3.9",
                   outLang: "ru",
                   addr: [{val: address}]
                },
                responseType: 'json'
            })
            return this.buildAddress(result.data)
        } catch (err) {
            return null
        }
    }

    private buildAddress(data: PochtaApiResponse): NormalizeResponse | null {
        let zip
        // !data.addr.accuracy.startsWith('3')
        if (data.addr.index) {
            zip = String(data.addr.index)
        }
        if (!data.addr.outaddr) {
            return null
        }
        return {
            address: data.addr.outaddr,
            zip
        }
    }
}
