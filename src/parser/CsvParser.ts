import * as parse from 'csv-parse';
import {Transform, TransformCallback} from 'stream';
import Axios from 'axios';
// import * as generate from 'csv-generate';

export class CsvParser {

    public async parseRemoteFile(url: string, addressColumnIndex: number): Promise<NodeJS.ReadableStream> {
        console.log('parseRemoteFile', url);
        const fileStream = await this.getRemoteFileStream(url);
        return this.parseStream(fileStream, addressColumnIndex);
    }

    private parseStream(fileStream: NodeJS.ReadableStream, addressColumnIndex: number): NodeJS.ReadableStream {
        return fileStream.pipe(parse({
            bom: true,
            delimiter: '\t',
            // columns: true,
            fromLine: 1,
            skipLinesWithError: true,
            encoding: 'utf8'
        })).pipe(this.getTransformStream(addressColumnIndex));


        // .on('data', async function (csvRow) {
        //     // if (csvRows.length > 1000) return
        //     csvRows.push(csvRow);
        // })
        // .on('end', async function () {
        //     resolve(csvRows)
        // })
        // .on('error', function (e) {
        //     reject(e)
        // })

        // const generator = generate({length: 20});
        // const parser = parse({bom: true, columns: true, ltrim: true, rtrim: true, quote: '"', delimiter: '\t'});

        // return fileStream.pipe(parser).pipe(transformer);
    }

    private async getRemoteFileStream(url: string): Promise<NodeJS.ReadableStream> {
        const response = await Axios({url, method: 'GET', responseType: 'stream' });
        return response.data;
    }

    private getTransformStream(addressColumnIndex: number) {
        return new Transform({
            objectMode: true,
            transform(row: string[], encoding: BufferEncoding, callback: TransformCallback) {
                // console.log('row', row)
                if (row.length > addressColumnIndex) {
                    return callback(null, [row[addressColumnIndex]]);
                }
                return callback(new Error('No column with such index'));
            },
            flush(callback: TransformCallback) {
                callback();
            }
        });
    }
}
