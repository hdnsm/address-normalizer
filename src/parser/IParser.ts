export interface IParser {
    parseRemoteFile(url: string): Promise<NodeJS.ReadableStream>
    parseStream(fileStream: NodeJS.ReadableStream): NodeJS.ReadableStream
}
