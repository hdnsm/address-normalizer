import { Router } from 'express';
import IndexController from '../controllers/index.controller';
import Route from '../routes.interface';
import {Uploader} from '../../uploader/Uploader';
import {RequestProcessor} from '../../processor/RequestProcessor';
import ApiController from '../controllers/api.controller';
import {RequestRepository} from '../../processor/RequestRepository';

class ApiRoute implements Route {
  public path = '/api';
  public router = Router();
  public apiController: ApiController

  constructor(uploader: Uploader, processor: RequestProcessor, repo: RequestRepository) {
    this.apiController = new ApiController(uploader, processor, repo);
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}/upload`, this.apiController.fileUpload);
    this.router.post(`${this.path}/requests`, this.apiController.createRequest);
    this.router.get(`${this.path}/requests`, this.apiController.getRequests);

    this.router.get(`${this.path}/currentUser`, this.apiController.currentUser);
    this.router.post(`${this.path}/login/account`, this.apiController.currentUser);
    this.router.post(`${this.path}/login/outLogin`, this.apiController.currentUser);
  }
}

export default ApiRoute;
