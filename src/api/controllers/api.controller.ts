import {NextFunction, Request, Response} from 'express';
import * as Busboy from 'busboy';
import {Uploader} from '../../uploader/Uploader';
import {RequestProcessor} from '../../processor/RequestProcessor';
import {RequestRepository} from '../../processor/RequestRepository';

class ApiController {

    constructor(private readonly uploader: Uploader,
                private readonly processor: RequestProcessor,
                private readonly repo: RequestRepository) {
    }

    public fileUpload = (req: Request, res: Response, next: NextFunction) => {
        try {
            const busboy = new Busboy({ headers: req.headers });
            let result: { requestId: string, url: string }
            let error: Error
            let remainingFilesCount = 0

            const sendResponse = () => {
                if (remainingFilesCount === 0) {
                    if (error) {
                        return next(error)
                    }
                    return res.json(result)
                }
            }

            busboy.on('file', async (fieldname, fileStream, filename, encoding, mimetype) => {
                // console.log('busboy on file')
                if (fieldname !== 'fileList') {
                    return
                }
                // TODO check extension and fileSize
                try {
                    remainingFilesCount++

                    result = await this.processor.uploadFile(filename, fileStream)
                } catch (err) {
                    error = err
                } finally {
                    remainingFilesCount--
                    sendResponse()
                }
            });

            busboy.on('finish', function() {
                console.log('busboy finish')
                sendResponse()
            });
            return req.pipe(busboy);
        } catch (error) {
            next(error);
        }
    }

    public createRequest = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const addressColumnIndex = Number(req.body.index) || null
            const requestId = String(req.body.requestId) || null
            const url = String(req.body.url) || null
            if (!addressColumnIndex || !requestId || !url) {
                res.status(400).end()
            }
            await this.processor.createAndPostRequest(requestId, url, addressColumnIndex - 1)
            res.status(201).end()
        } catch (err) {
            next(err);
        }
    }

    public getRequests = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const reqs = await this.repo.getAll()
            res.json({
                data: reqs,
                total: reqs.length
            })
        } catch (err) {
            next(err);
        }
    }

    public currentUser = (req: Request, res: Response) => {
        // if (!getAccess()) {
        //     res.status(401).send({
        //                              data: {
        //                                  isLogin: false,
        //                              },
        //                              errorCode: '401',
        //                              errorMessage: '请先登录！',
        //                              success: true,
        //                          });
        //     return;
        // }
        res.send({
                     name: 'Иван Иванов',
                     avatar: 'https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png',
                     userid: '00000001',
                     email: 'antdesign@alipay.com',
                     signature: '海纳百川，有容乃大',
                     title: '交互专家',
                     group: '蚂蚁金服－某某某事业群－某某平台部－某某技术部－UED',
                     tags: [
                         {
                             key: '0',
                             label: '很有想法的',
                         },
                         {
                             key: '1',
                             label: '专注设计',
                         },
                         {
                             key: '2',
                             label: '辣~',
                         },
                         {
                             key: '3',
                             label: '大长腿',
                         },
                         {
                             key: '4',
                             label: '川妹子',
                         },
                         {
                             key: '5',
                             label: '海纳百川',
                         },
                     ],
                     notifyCount: 12,
                     unreadCount: 11,
                     country: 'China',
                     access: 'admin',
                     geographic: {
                         province: {
                             label: '浙江省',
                             key: '330000',
                         },
                         city: {
                             label: '杭州市',
                             key: '330100',
                         },
                     },
                     address: '西湖区工专路 77 号',
                     phone: '0752-268888888',
                 });
    }

    public login = (req: Request, res: Response) => {
        const { password, username, type } = req.body;
        if (password === '1234' && username === 'admin') {
            res.send({
                         status: 'ok',
                         type,
                         currentAuthority: 'admin',
                     });
            return;
        }
        res.send({
                     status: 'error',
                     type,
                     currentAuthority: 'guest',
                 });
    }

    public logout = (req: Request, res: Response) => {
        res.send({ data: {}, success: true });
    }
}

export default ApiController;
