import Axios from 'axios'

export class Downloader {
    public async getRemoteFileStream(url: string): Promise<NodeJS.ReadableStream> {
        const response = await Axios({
            url,
            method: 'GET',
            responseType: 'stream'
        })
        return response.data
    }
}
