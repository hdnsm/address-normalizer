import * as AWS from 'aws-sdk';
import * as S3 from 'aws-sdk/clients/s3';
import * as path from 'path';
import {NormalizeRequest} from '../processor/RequestRepository';

// TODO move to ENV
const Bucket = 'address-normalizer';

export class Uploader {
    private readonly s3: S3
    constructor() {
        AWS.config.update({region: 'eu-central-1'});
        this.s3 = new AWS.S3()
    }

    public async init() {
        const listBucketsResult = await this.s3.listBuckets().promise()
        const found = listBucketsResult.Buckets.find((b: S3.Bucket) => {
            return b.Name === Bucket
        })
        if (!found) {
            await this.s3.createBucket({Bucket: Bucket}).promise()
        }
        console.log("Uploader successful init");
    }

    public async uploadInputFile(filename: string, requestId: string, fileStream: NodeJS.ReadableStream): Promise<S3.ManagedUpload.SendData> {
        const extname = path.extname(filename)
        const uploadParams = {
            ACL: "public-read",
            Bucket: Bucket,
            Key: `${requestId}_raw${extname}`,
            Body: fileStream
        }
        const uploadFileResult = await this.s3.upload(uploadParams).promise()
        // console.log('uploadFileResult', uploadFileResult)
        return uploadFileResult
    }

    public async uploadResultFile(req: NormalizeRequest, fileStream: NodeJS.ReadableStream): Promise<S3.ManagedUpload.SendData> {
        const extname = path.extname(req.inputFileUrl)
        const uploadParams = {
            ACL: "public-read",
            Bucket: Bucket,
            Key: `${req.requestId}_result${extname}`,
            Body: fileStream
        }
        const uploadFileResult = await this.s3.upload(uploadParams).promise()
        // console.log('uploadFileResult', uploadFileResult)
        return uploadFileResult
    }
}
