import 'dotenv/config';
import App from './app';
import validateEnv from './utils/validateEnv';
import IndexRoute from './api/routes/index.route';
import {Uploader} from './uploader/Uploader';
import {CsvParser} from './parser/CsvParser';
import {RequestProcessor} from './processor/RequestProcessor';
import {RequestRepository} from './processor/RequestRepository';
import * as redis from 'redis'
import {PochtaSdk} from './processor/PochtaSdk';
import ApiRoute from './api/routes/api.route';

validateEnv();

(async () => {
    const client = redis.createClient({
        host: 'redis',
    });
    const subscriber = redis.createClient({
        host: 'redis',
    });
    const publisher = redis.createClient({
        host: 'redis',
    });

    const uploader = new Uploader()
    await uploader.init()

    const csvParser = new CsvParser()
    const requestsRepo = new RequestRepository(client)
    const pochtaSdk = new PochtaSdk()
    const processor = new RequestProcessor(publisher, subscriber, requestsRepo, csvParser, uploader, pochtaSdk)

    const app = new App([
        new IndexRoute(),
        new ApiRoute(uploader, processor, requestsRepo)
    ]);

    app.listen();
})()
