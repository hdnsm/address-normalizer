#!/bin/bash
env
#docker pull hdnsm/address-normalizer:latest
#docker run -p 80:8000 -d hdnsm/address-normalizer:latest
cd address-normalizer
git fetch -a && git checkout master && git pull origin master
docker-compose -f docker-compose.yml up --force-recreate -d --build
